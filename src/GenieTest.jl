module GenieTest

using Dates
using Genie, Genie.Router

export runserver
function runserver()
    route("/") do
        "牛逼! My name is $(gethostname()). The current time is $(now())"
    end

    up(8000, async=false)
end

end # module
