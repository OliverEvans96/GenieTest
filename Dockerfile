FROM arm32v7/julia
COPY . /pkg
# Build the ARM image from an x86 host
COPY build/qemu-arm-static /usr/bin/
RUN /usr/bin/qemu-arm-static /usr/local/julia/bin/julia -e 'using Pkg; Pkg.add(PackageSpec(path="/pkg")); using GenieTest'
CMD julia -e 'using GenieTest; runserver()'
